package br.com.projuris.zipper.br.com.projuris.zipper.br.com.projuris.zipper.utils;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.squareup.okhttp.*;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;


public class SlackPostRequest {

    int statusCode;

    public int makePostRequest(String url, String json) {

        try {
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, json);
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response response = client.newCall(request).execute();
            System.out.println("sad");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this.statusCode;

    }


    public void populateObjectJsonWithColorAndHeader(JsonObject jsonSend, JsonArray jsonArrayFields,
                                                     String color, String title, String value) {
        jsonSend.addProperty("color", color);
        jsonArrayFields.add(createObjectHeader(title, value));
    }

    public JsonObject createObjectHeader(String title, String value) {
        JsonObject jsonObjectHeader = new JsonObject();
        jsonObjectHeader.addProperty("title", title);
        if(value != null) {
            jsonObjectHeader.addProperty("value", value);
        }
        jsonObjectHeader.addProperty("short", false);
        return jsonObjectHeader;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

}
