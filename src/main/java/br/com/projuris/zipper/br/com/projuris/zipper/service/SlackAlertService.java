package br.com.projuris.zipper.br.com.projuris.zipper.service;

import br.com.projuris.zipper.br.com.projuris.zipper.br.com.projuris.zipper.utils.SlackPostRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class SlackAlertService {


    @Value("${slack.successUrl}")
    public String successUrl;

    @Value("${slack.errorUrl}")
    public String errorUrl;

    private SlackPostRequest slackPostRequest = new SlackPostRequest();

    public void makePostSlackError(String title, String message) {
        JsonObject jsonSend = new JsonObject();
        JsonArray jsonArrayFields = new JsonArray();
        slackPostRequest.populateObjectJsonWithColorAndHeader(jsonSend, jsonArrayFields, "#f49200", title, message);
        jsonSend.add("fields", jsonArrayFields);
        slackPostRequest.makePostRequest(errorUrl, jsonSend.toString());
    }

    public void makePostSlackSuccess(String title, String message) {
        JsonObject jsonSend = new JsonObject();
        JsonArray jsonArrayFields = new JsonArray();
        slackPostRequest.populateObjectJsonWithColorAndHeader(jsonSend, jsonArrayFields, "#2885c2", title, message);
        jsonSend.add("fields", jsonArrayFields);
        slackPostRequest.makePostRequest(successUrl, jsonSend.toString());
    }
}
