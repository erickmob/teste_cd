package br.com.projuris.zipper.br.com.projuris.zipper.resources;

import br.com.projuris.zipper.br.com.projuris.zipper.service.BitbucketService;
import br.com.projuris.zipper.br.com.projuris.zipper.service.SlackAlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class RequestResource {

    private final Logger log = LoggerFactory.getLogger(RequestResource.class);

    @Autowired
    private BitbucketService bitbucketService;

    @Autowired
    private SlackAlertService slackAlertService;

    public RequestResource(BitbucketService bitbucketService) {
        this.bitbucketService = bitbucketService;
        this.slackAlertService = slackAlertService;
    }


    @RequestMapping("/merge")
    public void init(@RequestParam("issue") String issue,
                       @RequestParam("project") String project,
                       @RequestParam("key") String key,
                       @RequestParam("secret") String secret) {

        try {
            String accessToken = bitbucketService.getAccessToken(key, secret);
            String mergeUrl = bitbucketService.getPullRequestMergeUrl(accessToken, project, issue);

            if (isValidUrl(mergeUrl)) {
                bitbucketService.mergeIt(mergeUrl, accessToken);
                slackAlertService.makePostSlackSuccess(addIssuePrefix(issue, "SUCESSO"), "Merge para a master realizado com sucesso.");
            }else{
                slackAlertService.makePostSlackError(addIssuePrefix(issue, "URL de merge inválida."), mergeUrl);
            }
        } catch (Exception e) {
            e.printStackTrace();
            slackAlertService.makePostSlackError(addIssuePrefix(issue, e.getMessage()), e.getCause().toString());

        }
    }

    private String addIssuePrefix(@RequestParam("issue") String issue, String message) {
        return issue.concat(" - ").concat(message);
    }

    private boolean isValidUrl(String mergeUrl) {
        return mergeUrl != null && !mergeUrl.isEmpty();
    }
}
