package br.com.projuris.zipper.br.com.projuris.zipper.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.okhttp.*;
import com.squareup.okhttp.MediaType;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import java.io.IOException;

@Service
public class BitbucketService {

    @Value("${bitbucket.api.host}")
    private String bitbucketApiHost;

    @Value("${bitbucket.api.version}")
    private String bitbucketApiVersion;

    @Value("${bitbucket.api.accesTokenUrl}")
    private String bitbucketAccesTokenUrl;

    public String getAccessToken(String key, String secret) throws RuntimeException{
        String accessToken = null;
        String credentials = key.concat(":").concat(secret);
        String plainCreds = credentials;
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);

        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, "grant_type=client_credentials");
        Request request = new Request.Builder()
                .url("https://bitbucket.org/site/oauth2/access_token")
                .post(body)
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .addHeader("authorization", "Basic ".concat(base64Creds))
                .addHeader("cache-control", "no-cache")
                .build();

        try {
            Response response = client.newCall(request).execute();
            JsonObject convertedObject = new Gson().fromJson(response.body().string(), JsonObject.class);
            accessToken = convertedObject.get("access_token").getAsString();
        } catch (IOException e1) {
            e1.printStackTrace();
            throw  new RuntimeException("IOException ao pegar AccessToken", e1);
        } catch (Exception e2){
            e2.printStackTrace();
            throw  new RuntimeException("Exception ao pegar AccessToken", e2);
        }

        return accessToken;
    }

    public String getPullRequestMergeUrl(String accessToken, String project,  String issue){
        Integer pullRequestId = null;
        String mergeUrl = null;

        try {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(bitbucketApiHost.concat(bitbucketApiVersion).concat(generateFindPrIdUrl(accessToken, project, issue)))
                    .get()
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response response = client.newCall(request).execute();
            JsonObject convertedObject = new Gson().fromJson(response.body().string(), JsonObject.class);
            JsonObject values = null;
            if (convertedObject.getAsJsonArray("values").size() != 0) {
                values = (JsonObject)convertedObject.getAsJsonArray("values").get(0);
                pullRequestId = values.get("id").getAsInt();
                JsonObject links = (JsonObject) values.get("links");
                JsonObject merge = (JsonObject) links.get("merge");
                mergeUrl = merge.get("href").getAsString();
            }else{
                throw  new RuntimeException("Não achei URl de Merge no retorno do Bitbucket:".concat(convertedObject.toString()));
            }

        } catch (IOException e1) {
            e1.printStackTrace();
            throw  new RuntimeException("IOException ao pegar URl de Merge", e1);
        } catch (Exception e2){
            e2.printStackTrace();
            throw  new RuntimeException("Exception ao pegar URl de Merge", e2);
        }
        return mergeUrl;
    }

    public HttpStatus mergeIt(String mergeUrl, String accessToken){


        try {
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\n\t\"type\": \"String\",\n\t\"message\": \"Merged by Zipper.\"\n}");
            Request request = new Request.Builder()
                    .url(addAccessToken(mergeUrl, accessToken))
                    .post(body)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response response = client.newCall(request).execute();

            if(response.code() != 200){
                throw  new RuntimeException(response.body().string());
            }
        }  catch (IOException e1) {
            e1.printStackTrace();
            throw  new RuntimeException("IOException ao realizar Merge", e1);
        } catch (Exception e2){
            e2.printStackTrace();
            throw  new RuntimeException("Exception ao realizar Merge", e2);
        }

        return HttpStatus.OK;
    }

    private String addAccessToken(String mergeUrl, String accessToken) {
        return mergeUrl.concat("?access_token=").concat(accessToken);
    }


    private String generateFindPrIdUrl(String accessToken, String project, String issue) {
//        return "repositories/projuris_enterprise/"
        return "repositories/erickmob/"
                .concat(project)
                .concat("/pullrequests?")
                .concat("q=source.branch.name%3D%22").concat(issue).concat("%22")
                .concat("+AND+state+%3D+%22OPEN%22")
                .concat("&access_token=").concat(accessToken).concat("%3D");
    }

}
